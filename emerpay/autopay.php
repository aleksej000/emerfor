<?php
require dirname(__FILE__).'/_config.php';
require dirname(__FILE__).'/../classes/autopay.php';

$error_result = true;

if(isset($_GET['action'])) {
  $action = $_GET['action'];
  
  if($_GET['action'] == 'fields') {
    emerpay::loadModule($_GET['module']);
    $result = array('fields' => emerpay::getAutoFields(), 'types' => emerpay::getFieldTypes());
    echo json_encode($result);
    die();
  }
  elseif($_GET['action'] == 'add') {
    if(isset($_POST['save'])) {
      $p = new autopay();
      emerpay::loadModule($_POST['module']);
      $p->create($_POST['autopay'], $_POST['module'], $_POST['interval']);
      $_SESSION['result'] = true;
      $_SESSION['error'] = "Auto payment sucesfuly created";
      ob_clean();
      header("location: $base_url");
      die();
    }
  }
  elseif($_GET['action'] == 'run') {
    $p = new autopay();
    $p->run ((int)$_GET['id']);
    ob_clean();
    header("location: $base_url");
  }
  elseif($_GET['action'] == 'stop') {
    $p = new autopay();
    $p->stop ((int)$_GET['id']);
    ob_clean();
    header("location: $base_url");
  }
  elseif($_GET['action'] == 'del') {
    $p = new autopay();
    $p->delete((int)$_GET['id']);
    $_SESSION['result'] = true;
    $_SESSION['error'] = "Auto payment sucesfuly deleted";
    ob_clean();
    header("location: $base_url");
      die();
  }
  elseif($_GET['action'] == 'execute') {
    $p = new autopay();
    $p->execute((int)$_GET['id']);
    $_SESSION['result'] = true;
    $_SESSION['error'] = "Auto payment sucesfuly executed";
    ob_clean();
    header("location: $base_url");
    die();
  }
}
else {
  $p = new autopay();
  $autopays = $p->getList();
}

//Module
$module = 'autopay';

require dirname(__FILE__).'/templates/autopay.php';

$_SESSION['error'] = false;