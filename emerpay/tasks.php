<?php
require dirname(__FILE__).'/_config.php';
require dirname(__FILE__).'/../classes/tasks.php';
//TODO: Show task fields (AJAX)
//TODO: Execute task
//TODO: Delete task

$error_result = true;

if(isset($_GET['action'])) {
  $action = $_GET['action'];
  
  if($_GET['action'] == 'fields') {
    emerpay::loadModule($_GET['module']);
    $result = array('fields' => emerpay::getFields(), 'types' => emerpay::getFieldTypes());
    echo json_encode($result);
    die();
  }
  elseif($_GET['action'] == 'add') {
    if(isset($_POST['save'])) {
      $t = new tasks();
      emerpay::loadModule($_POST['module']);
      $t->create($_POST['task'], $_POST['module']);
      $_SESSION['result'] = true;
      $_SESSION['error'] = "Task sucesfuly created";
      ob_clean();
      header("location: $base_url");
      die();
    }
  }
  elseif($_GET['action'] == 'execute') {
    $t = new tasks();
    $task = $t->get($id);
    if($task['ammount'] <= $balance) {
      $t->execute((int)$_GET['id']);
      $_SESSION['result'] = true;
      //TODO: details
      $_SESSION['error'] = "Task executed";
      ob_clean();
      header("location: $base_url");
      die();
    }
    else {
      $_SESSION['result'] = false;
      $_SESSION['error'] = "insufficient funds";
      ob_clean();
      header("location: $base_url");
      die();
    }
  }
  elseif($_GET['action'] == 'del') {
    $t = new tasks();
    $t->delete((int)$_GET['id']);
    $_SESSION['result'] = true;
    $_SESSION['error'] = "Task sucesfuly deleted";
    ob_clean();
    header("location: $base_url");
      die();
  }
}
else {
  $t = new tasks();
  if(isset($_GET['page']))
    $page = (int)$_GET['page'];
  else
    $page = 1;
  $tasks = $t->getList('1', $page, 25);
}

//Module
$module = 'tasks';

require dirname(__FILE__).'/templates/tasks.php';

$_SESSION['error'] = false;