<?php
if(!isset($emerpay_app)) die('Can not call directly !');

$fn_users = function($data) {
  $FAH_URL = "http://kakaostats.com/t.php?t=43003";
  /**
   * This function must return array of users in a form of 
   * array(username => array(EmercoinAddress,ammount)
   */
  $points = $data['Points for 1 EMC'];
  

  function curl($url){
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
      $data = curl_exec($ch);
      curl_close($ch);
      return $data;
  }


  $html = curl($FAH_URL);

  $matches = array();
  $res = preg_match_all("/<a[^>]+class=\"c[^>]+>(.+?)<\/a>.*?<td[^>]*?>([^<]*?)<\/td><\/tr>/mi", $html, $matches, PREG_SET_ORDER);

  for($i = 0; $i < count($matches); $i++) {
    $user = preg_split("/[ _]/", $matches[$i][1]);
    $user[1] = str_replace(array('<wbr>', '</wbr>'), '',  html_entity_decode($user[1]) );
    $user[1] = str_replace('?', '', $user[1] );
    $user[2] = (int)  str_replace(array(' ',','), '', $matches[$i][2]);
    if(strlen($user[1]) > 30)
      $users[$user[0]] = array($user[1], $user[2]/$points);
  }
  
  foreach($users as $username => $user) {
    $u = new users();
    //Getting total users payed value
    $payed = (double)$u->getData($username);
    //Difference
    $total = (double)$user[1];
    //Saving current points
    $pay = $total - $payed;
    if($pay < 0) $pay = 0;
    $users[$username][1] = $pay;
    //$u->setData($username, $total);
  }
  
  return $users;

};

//After task has been automaticaly created by Autopay,
// the autopay data [$data] will be parsed to the task data
$fn_on_auto_create = function($data, $autopay_id) {
  $data['Date'] = date("Y-m-d");
  $data['auto'] = $autopay_id;
  //...
  return $data;
};

//Before task gets executed
$fn_before_execute = function($data, $id) {
  //...
};

//After payment sucessfuly completed
$fn_on_payment = function($data) {
    $username = $data['username'];
    $address = $data['address'];
    $ammount = $data['ammount'];
    $u = new users();
    $payed = (double)$u->getData($username);
    $total = $payed + $ammount;
    $u->setData($username, $total);
};

$fn_task_title = function($data) {
  $date = $data['Date'];
  $points = $data['Points for 1 EMC'];
  if(isset($data['auto'])) {
    return "AUTOPAY (Date: $date; $points points for EMC)";
  }
  else {
    return "Date: $date; $points points for EMC";
  }
};

$fn_autopay_title = function($data) {
  $date = $data['Date'];
  $points = $data['Points for 1 EMC'];
  return "$points points for EMC";
};

return array(
    //Fields, showen, when task has been created
    'fields' => array(
        'Points for 1 EMC' => '1000',
        'Date' => date("Y-m-d")
    ),
    //Fields, showen, when Autopay task has been created
    'autopay_fields' => array(
        'Points for 1 EMC' => '1000'
    ),
    //Supported date,int,float, text by default
    'field_types' => array(
        'Points for 1 EMC' => 'int',
        'Date' => 'date'
    ),
    'users' => $fn_users,
    'task_title' => $fn_task_title,
    'fn_before_execute' => $fn_before_execute,
    'autopay_title' => $fn_autopay_title,
    'fn_on_auto_create' => $fn_on_auto_create,
    
    'on_payment' => $fn_on_payment
);