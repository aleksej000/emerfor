<?php
require dirname(__FILE__).'/_config.php';
require dirname(__FILE__).'/../classes/users.php';

$u = new users();
if(isset($_GET['page']))
  $page = (int)$_GET['page'];
else
  $page = 1;
$users = $u->getList('1', $page, 25);

//Module
$module = 'users';


require dirname(__FILE__).'/templates/users.php';