<?php
require dirname(__FILE__).'/_config.php';

$st = $connection->query("SELECT `password`, email FROM admin");
if($st) {
  $st->setFetchMode(PDO::FETCH_NUM);
  if ($row = $st->fetch())
    $old_pass = $row[0];
    $email = $row[1];
  $st->closeCursor();
}

if(isset($_POST['save'])) {
  $err = false;
  $err_field = '';
  
  if(($_POST['old_pass'] != '') && ($_POST['pass'] != '')) {
      if(!isset($old_pass)) {
        $err = 'Database error';
      }
      elseif($old_pass != md5(trim($_POST['old_pass']))) {
        $err = 'Old password is wrong';
        $err_field = 'pass0';
        //var_dump($old_pass, $_POST['old_pass']);
      }
      elseif(strlen(trim($_POST['pass'])) < 5) {
        $err = 'The length of the new password is less than 5 symbols';
        $err_field = 'pass1';
      }
      elseif(trim($_POST['pass']) != trim($_POST['pass2'])) {
        $err = 'Passwords don\'t match';
        $err_field = 'pass2';
      }
      
      if($err === false) {
        $password = trim($_POST['pass']);
        //Changing password
        $st = $connection->prepare('UPDATE admin SET `password` = MD5(?)');
        if( ! $st->execute(array($password)) ) {
          $err = 'Database error';
        }
      }
  }

  $email = trim($_POST['email']);
  //Changing email
  $st = $connection->prepare('UPDATE admin SET email = ?');
  if( ! $st->execute(array($email)) ) {
    $err = 'Database error';
  }
}

//Module
$module = 'admin';


require dirname(__FILE__).'/templates/admin.php';