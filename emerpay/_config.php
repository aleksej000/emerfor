<?php
error_reporting(E_ALL^E_NOTICE^E_WARNING^E_STRICT^E_DEPRECATED);
ini_set("display_errors", true);
$application = 'f@h';

//If testmode - then no payments are made
define('TESTMODE', false);

require dirname(__FILE__).'/../db.php'; 
require dirname(__FILE__).'/../db+.php';
require dirname(__FILE__).'/_login.php';
require(dirname(__FILE__).'/../classes/emercoin.php');
require(dirname(__FILE__).'/../classes/emercoin.conf.php');
require_once(dirname(__FILE__).'/../classes/emerpay.php');

$account = 'Emer_for_free';//'FAH';
$base_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
$task_module = $_GET['module'];

$modules = emerpay::listModules();
foreach($modules as $module => $file) {
  $first_module = $module;
  break;
}

date_default_timezone_set('Europe/Riga');
    
$balance = emercoin::getAccauntBalance($account);