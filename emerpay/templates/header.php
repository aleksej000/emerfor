<?php
header("encoding: utf8;");
?>
<!DOCTYPE html>
<html lang="en">
  
<head>
  <meta charset="utf-8">
  <title>
  </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="../css/bootstrap.min.css" rel="stylesheet">
  <link rel="shortcut icon" href="http://emerfor.org/favicon.ico" />
  <script src="../js/jquery.min.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</head>

<body>
  <div class="container">
    <nav class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">ACCOUNT BALANCE: <?=$balance?> EMC</a>
      </div>
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav">
          <li <?if($module=='users'):?>class="active"<?endif;?>>
            <a href="users.php">Users</a>
          </li>
          <li <?if($module=='tasks'):?>class="active"<?endif;?>>
            <a href="tasks.php">Tasks</a>
          </li>
          <li <?if($module=='payments'):?>class="active"<?endif;?>>
            <a href="payments.php">Payments</a>
          </li>
          <li <?if($module=='autopay'):?>class="active"<?endif;?>>
            <a href="autopay.php">Autopay</a>
          </li>
          <li <?if($module=='admin'):?>class="active"<?endif;?>>
            <a href="admin.php">Password and email</a>
          </li>
          <li>
            <a href="logout.php">Logout</a>
          </li>
        </ul>
      </div>
    </nav>
    <?if(isset($_SESSION['error']) && $_SESSION['error'] !== false):?>
      <?if(!$_SESSION['result']):?>
      <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Error!</strong> <?=$_SESSION['error']?>
      </div>
      <?else:?>
      <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <?=$_SESSION['error']?>
      </div>
      <?endif;?>
    <?endif;?>
  </div>