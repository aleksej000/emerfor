<? require 'header.php';?>
    <script src="../js/fields.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <link href="../css/datepicker.css" rel="stylesheet">
    <div class="container" style="width: 1000px;">
      
      <?if(!isset($action)):?>
      
      <a href='?action=add' class='btn btn-info' style='margin-bottom: 8px;'>NEW Task</a>
      
      <table class="table">
        <tbody>
          <tr>
            <th>
              Task
            </th>
            <th>
              Ammount
            </th>
            <th>
              Payed
            </th>
            <th>
              
            </th>
          </tr>
          <?foreach($tasks as $task):?>
          <tr>
            <td>
              <a href="payments.php?task=<?=$task['id']?>"><?=$task['name']?></a>
            </td>
            <td>
              <?=$task['ammount']?>
            </td>
            <td>
              <?=$task['executed']?>
            </td>
            <td align='right'>
              <?if($task['error'] > 0):?>
                <a href="<?=$base_url?>?action=execute&id=<?=$task['id']?>" class="btn btn-success">PAY</a>
                <a href="<?=$base_url?>?action=del&id=<?=$task['id']?>" class="btn btn-danger btn-confirm">Delete</a>
              <?endif;?>
              <?if($task['error'] > 0):?>
                <a href="payments.php?task=<?=$task['id']?>" class="btn btn-danger btn-error">Errors (<?=$task['error']?>)</a>
              <?endif;?>
            </td>
          </tr>
          <?endforeach;?>
        </tbody>
      </table>
      
      <?if(db::$pages > 1):?>
      <center>
        <ul class="pagination">
          <?if($page != 1):?>
          <li>
            <a href="<?=$base_url?>?page=1">First</a>
          </li>
          <?endif;?>
          <? foreach(db::$pages_list AS $p):?>
          <li <?if($page==$p):?> class="active"<?endif;?>>
            <a href="<?=$base_url?>?page=<?=$p?>"><?=$p?></a>
          </li>
          <?endforeach;?>
          <li>
            <a href="<?=$base_url?>?page=<?=db::$pages?>">Last</a>
          </li>
        </ul>
      </center>
      <?endif;?>
      
      <?elseif($action == 'add'):?>
      <style>
        .w100,.w200,.w300,.w400 {
          display: inline-block;
        }
        .w100 {
          width: 120px;
        }
        .w200 {
          width: 200px;
        }
        .w300 {
          width: 300px;
        }
        .w400 {
          width: 400px;
        }
      </style>
      
      <div class="well">
        <form method="post" action="<?=$base_url?>?action=add">
          <div>
            <label class="w100">Task type</label>
            <select name="module" id="module">
              <?foreach($modules as $module => $file):?>
              <option value="<?=$module?>" <?if($module == $first_module) echo "SELECTED";?>><?=$module?></option>
              <?endforeach;?>
            </select>
          </div>
          <div id ="fields"></div>
          <input type="submit" name='save' value="Save" class="btn btn-success" style="margin-left: 780px; width: 120px;"/>
        </form>
      </div>
      
      <script type="text/javascript">
        $('#module').change(function() {
          var module = $(this).val()
          $.getJSON('<?=$base_url?>?action=fields&module='+module, function(data) {
            var html = ''
            for(var i in data.fields) {
              html += '<div class="form-group w300">'
              if((typeof data.fields[i]) == 'string') {
                html += '<label class="w100">'+i+'</label> <input name="task['+i+']" value="'+data.fields[i]+'" data-type="'+data.types[i]+'"/> '
              }
              else if(data.fields[i].constructor === Object) {
                var elValues = data.fields[i]
                html += '<label class="w100">'+i+'</label> <select name="task['+i+']">'
                for(var j in elValues)
                  html += '<option value="'+elValues[j]+'">'+j+'</option>'
                html += '</select>'
              }
              html += '</div>'
            }
            $('#fields').html(html)
            showFields()
          })
        })
        $('#module').trigger('change')
      </script>
      <?endif;?>
      
    </div>

<script type="text/javascript">
  $('.btn-confirm').click(function() {
    return confirm('Delete this task ?')
  })
</script>

<? require 'footer.php';?>