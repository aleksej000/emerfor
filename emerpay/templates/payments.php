<? require 'header.php';?>
    <div class="container" style="width: 1000px;">
      <div class='well'>
        <form id='filterform'>
        <table>
          <tr>
            <th width='100'>Task:</th>
            <td width='600'> 
              <select name='task' id='task'>
                <option value='0'> ... </option>
                <?foreach($tasks as $task):?>
                <option value='<?=$task['id']?>' <?if($_REQUEST['task'] == $task['id']) echo 'SELECTED';?>><?=$task['name']?> [<?=$task['ammount']?>]</option>
                <?endforeach;?>
              </select> 
            </td>
            <th width='100'>User:</th>
            <td>
              <select name='user' id='user'>
                <option value='0'> ... </option>
                <?foreach($users as $user):?>
                <option value='<?=$user['id']?>' <?if($_REQUEST['user'] == $user['id']) echo 'SELECTED';?>><?=$user['name']?></option>
                <?endforeach;?>
              </select> 
            </td>
          </tr>
        </table>
        </form>
      </div>
      
      <table class="table">
        <tbody>
          <tr>
            <th>
              Task
            </th>
            <th>
              Username
            </th>
            <th>
              Address
            </th>
            <th>
              Ammount
            </th>
            <th>
            </th>
          </tr>
          <?foreach($payments as $payment):?>
          <tr>
            <td>
              <?=$payment['task']?>
            </td>
            <td>
              <?=$payment['username']?>
            </td>
            <td>
              <?=$payment['address']?>
            </td>
            <td>
              <?=$payment['ammount']?>
            </td>
            <td>
              <?if(strtolower($payment['error']) == 'ok'):?>
                <a href="#" class="btn btn-success btn-error">OK</a>
              <?elseif($payment['error'] != null):?>
              
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Error description</h4>
                      </div>
                      <div class="modal-body">
                                      <?=$payment['error']?>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              
                <a href="#" class="btn btn-danger btn-error">Error</a>
              <?endif;?>
            </td>
          </tr>
          <?endforeach;?>
        </tbody>
      </table>
      
      <?if(db::$pages > 1):?>
      <center>
        <ul class="pagination">
          <?if($page != 1):?>
          <li>
            <a href="payments.php?page=1">First</a>
          </li>
          <?endif;?>
          <? foreach(db::$pages_list AS $p):?>
          <li <?if($page==$p):?> class="active"<?endif;?>>
            <a href="payments.php?page=<?=$p?>"><?=$p?></a>
          </li>
          <?endforeach;?>
          <li>
            <a href="payments.php?page=<?=db::$pages?>">Last</a>
          </li>
        </ul>
      </center>
      <?endif;?>
      
    </div>

<script type="text/javascript">
  $('#user,#task').change(function() {
    $('#filterform').submit()
  })
  
  $('.btn-error').click(function() {
    $(this).prev().modal({show: true})
    return false
  })
</script>

<? require 'footer.php';?>