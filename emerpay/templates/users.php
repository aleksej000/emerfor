<? require 'header.php';?>
    <div class="container" style="width: 600px;">
      <table class="table">
        <tbody>
          <tr>
            <th>
              Name
            </th>
            <th>
              Address
            </th>
            <th>
              Info
            </th>
          </tr>
          <?foreach($users as $user):?>
          <tr>
            <td>
              <?=$user['name']?>
            </td>
            <td>
              <?=$user['address']?>
            </td>
            <td>
              <?=$user['additional']?>
            </td>
          </tr>
          <?endforeach;?>
        </tbody>
      </table>
      
      <?if(db::$pages > 1):?>
      <center>
        <ul class="pagination">
          <?if($page != 1):?>
          <li>
            <a href="users.php?page=1">First</a>
          </li>
          <?endif;?>
          <? foreach(db::$pages_list AS $p):?>
          <li <?if($page==$p):?> class="active"<?endif;?>>
            <a href="users.php?page=<?=$p?>"><?=$p?></a>
          </li>
          <?endforeach;?>
          <li>
            <a href="users.php?page=<?=db::$pages?>">Last</a>
          </li>
        </ul>
      </center>
      <?endif;?>
      
    </div>
<? require 'footer.php';?>