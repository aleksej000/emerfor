<? require 'header.php';?>
  <script src="../js/fields.js"></script>
    <script src="../js/bootstrap-datepicker.js"></script>
    <link href="../css/datepicker.css" rel="stylesheet">
  
    <div class="container" style="width: 1000px;">
      
      <?if(!isset($action)):?>
      
      <a href='?action=add' class='btn btn-info' style='margin-bottom: 8px;'>NEW Autopay</a>
      
      <table class="table">
        <tbody>
          <tr>
            <th>
              Autopay
            </th>
            <th style='text-align: center'>
              Interval
            </th>
            <th style='text-align: center'>
              Executed times
            </th>
            <th style='text-align: center'>
              Last executed
            </th>
            <th></th>
          </tr>
          <?foreach($autopays as $autopay):?>
          <tr>
            <td>
              <?=$autopay['name']?>
            </td>
            <td style='text-align: center'>
              <?=$autopay['interval']?>
            </td>
            <td style='text-align: center'>
              <?=$autopay['ex_times']?>
            </td>
            <td style='text-align: center'>
              <?=$autopay['last_ex']?>
            </td>
            <td style='text-align: right'>
                <a href="<?=$base_url?>?action=del&id=<?=$autopay['id']?>" class="btn btn-danger btn-confirm">Delete</a>
            </td>
          </tr>
          <?endforeach;?>
        </tbody>
      </table>
      
      
      <?elseif($action == 'add'):?>
      <style>
        .w100,.w200,.w300,.w400 {
          display: inline-block;
        }
        .w100 {
          width: 120px;
        }
        .w200 {
          width: 200px;
        }
        .w300 {
          width: 300px;
        }
        .w400 {
          width: 400px;
        }
      </style>
      
      <div class="well">
        <form method="post" action="<?=$base_url?>?action=add">
          <div>
            <label class="w100">Period</label>
            <select name="interval" id="interval">
              <option value="day">Day</option>
              <option value="week">Week</option>
              <option value="month">Month</option>
            </select>
          </div>
          <div>
            <label class="w100">Autopay type</label>
            <select name="module" id="module">
              <?foreach($modules as $module => $file):?>
              <option value="<?=$module?>" <?if($module == $first_module) echo "SELECTED";?>><?=$module?></option>
              <?endforeach;?>
            </select>
          </div>
          <div id ="fields"></div>
          <input type="submit" name='save' value="Save" class="btn btn-success" style="margin-left: 780px; width: 120px;"/>
        </form>
      </div>
      
      <script type="text/javascript">
        $('#module').change(function() {
          var module = $(this).val()
          $.getJSON('<?=$base_url?>?action=fields&module='+module, function(data) {
            var html = ''
            for(var i in data.fields) {
              html += '<div class="form-group w300">'
              if((typeof data.fields[i]) == 'string') {
                html += '<label class="w100">'+i+'</label> <input name="autopay['+i+']" value="'+data.fields[i]+'" data-type="'+data.types[i]+'"/> '
              }
              else if(data.fields[i].constructor === Object) {
                var elValues = data.fields[i]
                html += '<label class="w100">'+i+'</label> <select name="autopay['+i+']">'
                for(var j in elValues)
                  html += '<option value="'+elValues[j]+'">'+j+'</option>'
                html += '</select>'
              }
              html += '</div>'
            }
            $('#fields').html(html)
            showFields()
          })
        })
        $('#module').trigger('change')
      </script>
      <?endif;?>
      
    </div>

<script type="text/javascript">
  $('.btn-confirm').click(function() {
    return confirm('Delete this Autopay ?')
  })
</script>

<? require 'footer.php';?>