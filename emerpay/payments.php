<?php
require dirname(__FILE__).'/_config.php';
require dirname(__FILE__).'/../classes/payments.php';

$p = new payments();
if(isset($_GET['page']))
  $page = (int)$_GET['page'];
else
  $page = 1;

$condition = ' TBL.ammount > 0 ';
if(isset($_REQUEST['task']) && $_REQUEST['task'] != '0')
  $condition .= ' AND task_id = '.(int)$_REQUEST['task'];
if(isset($_REQUEST['user']) && $_REQUEST['user'] != '0')
  $condition .= ' AND user_id = '.(int)$_REQUEST['user'];

$payments = $p->getList($condition, $page, 25);

$t = new tasks();
$tasks = $t->getAllList();

$u = new users();
$users = $u->getAllList();

//Module
$module = 'payments';


require dirname(__FILE__).'/templates/payments.php';