<?php
if(!isset($application)) die("Can not call directly");
if(!isset($connection)) die('Variable $connection is not set');

class db {
  
  public static $pages = 1;
  public static $pages_range = 3;
  public static $pages_list;
  
  static public function eacape($value) {
   global $connection;
   return $connection->quote($value);
  }
  
  static public function execute($sql) {
    global $connection;
    return $connection->query($sql);
  }

  /**
   * List of Rows with named indexes
   * @param type $sql
   * @return boolean
   */
  static public function getList($sql) {
   global $connection;
   $st = $connection->query($sql);
   if($st) {
     $st->setFetchMode(PDO::FETCH_ASSOC);
     $result = array();
     while ($row = $st->fetch())
       $result[] = $row;
     $st->closeCursor();
     return $result;
   }
   return false;
  }

  /**
   * Row with named indexes
   * @param type $sql
   * @return boolean
   */
  static public function getRow($sql) {
   global $connection;
   $st = $connection->query($sql);
   if($st) {
     $st->setFetchMode(PDO::FETCH_ASSOC);
     if ($row = $st->fetch()) {
       $result = $row;
     }
     else {
       $result = false;
     }
     $st->closeCursor();
     return $result;
   }
   return false;
  }

  /**
   * Value
   * @param type $sql
   * @return boolean
   */
  static public function getValue($sql) {
   global $connection;
   $st = $connection->query($sql);
   if($st) {
     $st->setFetchMode(PDO::FETCH_NUM);
     if ($row = $st->fetch()) {
       $result = $row[0];
     }
     $st->closeCursor();
     return $result;
   }
   return false;
  }

  /**
   * Get data from page
   * 
   * @param type $sql - SELECT SQL with condition
   * @param type $table - table name
   * @param type $condition - condition for table
   * @param type $page
   * @param type $per_page
   */
  static public function getPage($sql, $table, $condition, $page = 1, $per_page = 25) {
    global $connection;
    $count = self::getValue("SELECT COUNT(*) FROM $table TBL WHERE $condition");
    
    self::$pages = (int)ceil($count/$per_page);
    $page = (int)$page;
    if($page < 1) $page = 1;
    elseif($page > self::$pages) $page = self::$pages;

    $from = ($page - 1)*$per_page;

    $pages_from = $page - self::$pages_range;
    $pages_to = $page + self::$pages_range;
    if($pages_from < 1) $pages_from = 1;
    if($pages_to > self::$pages) $pages_to = self::$pages;
    if($pages_to < 1) $pages_to = 1;
    
    self::$pages_list = array();
    for($i = $pages_from; $i <= $pages_to; $i++) {
      self::$pages_list[] = $i;
    }
    $sql = $sql." WHERE $condition LIMIT $from, $per_page"; //echo $sql;
    return self::getList($sql);
  }
  
  static public function insert($table, $data) {
    global $connection;
    foreach ($data as $field => $value) {
      $fields[] = '`'.$field.'`';
      $values[] = $connection->quote($value);
    }
    
    $fields = implode(", ", $fields);
    $values = implode(", ", $values);
    
    $sql = "INSERT INTO $table ($fields) VALUES ($values)";
    return $connection->query ($sql);
  }
  
  static public function replace($table, $data) {
    global $connection;
    foreach ($data as $field => $value) {
      $fields[] = $field;
      $values[] = $connection->quote($value);
    }
    
    $fields = implode(", ", $fields);
    $values = implode(", ", $values);
    
    $sql = "REPLACE INTO $table ($fields) VALUES ($values)";
    return $connection->query ($sql);
  }
  
  static public function update($table, $data, $condition) {
    global $connection;
    
    if(is_array($data)) {
      foreach ($data as $field => $value) {
        $values[] = "$field = ".$connection->quote($value);
      }
      $values = implode(", ", $values);
    }
    else {
      $values = $data;
    }
    
    $sql = "UPDATE $table SET $values WHERE ".$condition;
    return $connection->query ($sql);
  }
  
  static public function getLastId($table) {
    return self::getValue("SELECT last_insert_id() FROM `$table`");
  }  

}