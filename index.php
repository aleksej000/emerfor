<?php
error_reporting(E_ALL^E_NOTICE^E_WARNING^E_STRICT^E_DEPRECATED);
ini_set("display_errors", true);
$run_service = true;
$application = 'EMERFOR';

//Initializing database and session
require(dirname(__FILE__).'/db.php');
require(dirname(__FILE__).'/classes/emercoin.php');
require(dirname(__FILE__).'/classes/emercoin.conf.php');
//require(dirname(__FILE__).'/classes/captcha.php');
header("encoding: utf8;");
global $connection;
require(dirname(__FILE__).'/gplus.php');

//Account name
$account = 'Emer_for_free';
$balance = emercoin::getAccauntBalance($account);
$home_address = emercoin::getAccountAddress($account);
/*
//Captcha №1
$text = <<<TEXT
Mumblecore whatever irony photo booth Kale chips XOXO shabby chic
freegan banjo locavore vinyl gentrify mumblecore next level VHS
Swag Pitchfork flexitarian Bushwick gastropub Bespoke DIY flannel
try-hard stumptown chambray asymmetrical scenester Portland beard
bitters plaid polaroid Terry Richardson fap gluten-free trust fund
tattooed authentic umami forage Fingerstache pork belly authentic
DIY Bushwick tote bag Gastropub beard Etsy jean shorts keffiyeh
biodiesel seitan quinoa literally sriracha dreamcatcher
lomo aesthetic craft beer shabby chic
TEXT;

$text = str_replace(PHP_EOL, ' ', $text);
$text = explode(' ', $text);
$len = count($text);
for($i = 0; $i < 3; $i++)
  $captcha_1[] = $text[rand(0, $len)];
$captcha_1 = implode('-', $captcha_1);
$captcha_1 = str_replace(' ', '', $captcha_1);

$capcha_file_1 = 'captcha_1_'.session_id().'.png';
$capcha_dir = dirname(__FILE__).'/i/';

array_map('unlink', glob($capcha_dir.'captcha*.png'));

CAPTCHA__X($capcha_dir.$capcha_file_1, $captcha_1, 50, 500, dirname(__FILE__).'/fonts/arial.ttf');

//Captcha №2
for($i = 0; $i < 5; $i++)
  $numbers[] = rand(0, 9999) - 5000;

$captcha_2_num = 0;
for($i = 0; $i < 5; $i++) {
  $captcha_2_num += $numbers[$i];
  if($numbers[$i] >= 0)
    $captcha_2 .= '+'.$numbers[$i];
  else
    $captcha_2 .= $numbers[$i];
}

$capcha_file_2 = 'captcha_2_'.session_id().'.png';
$capcha_dir = dirname(__FILE__).'/i/';

CAPTCHA__X($capcha_dir.$capcha_file_2, $captcha_2, 50, 500, dirname(__FILE__).'/fonts/arial.ttf');

//Captcha №3
$captcha_3 = $captcha_2_num + strlen($captcha_1);
*/

if(isset($_POST['save'])) {
  $address = $connection->quote($_POST['address']);
  $name = $connection->quote($_POST['name']);
  $ip = $connection->quote($_SERVER['REMOTE_ADDR']);
  $gplus = $connection->quote($_SESSION['gplus.id']);
  $ammount = $connection->quote(10);
  
  $st = $connection->query("SELECT COUNT(*) FROM payments WHERE IP = $ip");
  if($st) {
    $st->setFetchMode(PDO::FETCH_NUM);
    if ($row = $st->fetch()) {
      $ip_count = (int)$row[0];
    }
    $st->closeCursor();
  }
  
  $st = $connection->query("SELECT COUNT(*) FROM payments WHERE address = $address");
  if($st) {
    $st->setFetchMode(PDO::FETCH_NUM);
    if ($row = $st->fetch()) {
      $address_count = (int)$row[0];
    }
    $st->closeCursor();
  }
  
  $st = $connection->query("SELECT COUNT(*) FROM payments WHERE gplus = $gplus");
  if($st) {
    $st->setFetchMode(PDO::FETCH_NUM);
    if ($row = $st->fetch()) {
      $gplus_count = (int)$row[0];
    }
    $st->closeCursor();
  }
  
  
  if(!$run_service) {
    $err = true;
    $out_text[] = "Service temporary unavailable";
  }
  elseif(!isset($_POST['name']) || (trim($_POST['name']) == '')) {
    $err = true;
    $out_text[] = "Empty name";
  }
  elseif(strpos(strtolower(trim($_POST['name'])), 'yarow') !== false) {
    $err = true;
    $out_text[] = "No Yarowrath (Velesov_Lox) !!!";
  }
  elseif(strpos(strtolower(trim($_POST['name'])), 'jarow') !== false) {
    $err = true;
    $out_text[] = "No Jarowrat (Velesov_Lox) !!!";
  }
  elseif(strpos(strtolower(trim($_POST['name'])), 'yarov') !== false) {
    $err = true;
    $out_text[] = "No Yarovrath (Velesov_Lox) !!!";
  }
  elseif(strpos(strtolower(trim($_POST['name'])), 'jarov') !== false) {
    $err = true;
    $out_text[] = "No Jarovrat (Velesov_Lox) !!!";
  }
  elseif(!isset($_POST['address']) || (trim($_POST['address']) == '')) {
    $err = true;
    $out_text[] = "Empty address";
  }
  /*elseif($_SESSION['captcha_1'] != $_POST['captcha_1']) {
    $err = true;
    $out_text[] = "Wrong capcha #1";
  }  
  elseif($_SESSION['captcha_2'] != $_POST['captcha_2']) {
    $err = true;
    $out_text[] = "Wrong capcha #2";
  }  
  elseif($_SESSION['captcha_3'] != $_POST['captcha_3']) {
    $err = true;
    $out_text[] = "Wrong capcha #3";
  }  */
  elseif($gplus_count > 0) {
    $err = true;
    $out_text[] = "Your Google+ account ($gplus) already reasieved emercoins";
  }  
  elseif($ip_count > 0) {
    $err = true;
    $out_text[] = "Your IP ($ip) already reasieved emercoins";
  }
  elseif($address_count > 0) {
    $err = true;
    $out_text[] = "Your address ($address) already reasieved emercoins";
  }
  elseif($balance < 10.01) {
    $err = true;
    $out_text[] .= "Insufficient funds";
  }
  else {
    try {
      emercoin::sendToAddress($_POST['address'], 10, $account);
      $err = false;
      $out_text[] = 'Emercoins has been sent to your address';
      $connection->exec("INSERT INTO payments (address, gplus, name, ip, ammount) VALUES ($address, $gplus, $name, $ip, $ammount)");      
    } catch (Exception $exc) {
      $err = false;
      $display_err = jsonRPCClient::$error;
      $out_text[] = "Payment error [$display_err[message] (code: $display_err[code])]";
    }


  }
  
  $out_text = implode('<br>', $out_text);
  
}

//Selecting and displaying products
$st = $connection->query("SELECT `id`, `name`, `IP`, `address`, `ammount`, DATE_FORMAT(`date`, '%d.%m.%Y %H:%i') AS `date` FROM `payments` ORDER BY id DESC");
if($st) {
  $st->setFetchMode(PDO::FETCH_NAMED);
  while ($row = $st->fetch())
    $payments[] = $row;
  $st->closeCursor();
}



$_SESSION['captcha_1'] = $captcha_1;
$_SESSION['captcha_2'] = (int)$captcha_2_num;
$_SESSION['captcha_3'] = (int)$captcha_3;

//Donate EQLYz6CUoqwLuyc8EyPTom7jnSGNomKXbu
?>
<!DOCTYPE html>
<html lang="en">
<head>    
  <meta charset="utf-8">
  <title>Emercoins for free</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="/favicon.ico">
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <style>
    .tblMain th,.tblMain td {
      padding: 4px;
    }
    .tblMain input {
      width: 400px;
    }
    .tblMain input[type=submit] {
      text-align: center;
    }
    
    .alert {
      width: 600px;
    }
    
    .table {
      width: 1000px;
    }
    .table th {
      text-align: center;
    }
  </style>
</head>
<body>

<center>

  <table width="90%"> 
    <tr>
      <td>
        <a href="http://emercoin.com">
          <img src="i/emercoin_1.png"/>
        </a>
      </div>
      <td align="right" valign="top">
        <h4>Donate emercoins to address: <?=$home_address?></h4>
        <h2>Total sum: <span style="color:black; background-color: #9999ff;"><?=$balance?> EMC</span></h2>
      </td>
    </tr>
  </table>
  
  <h1>Receive 10 emercoins for free</h1>
    <?if(isset($_SESSION['gplus'])):?>
    <form method="post">
      <table class="tblMain">
        <tr>
          <th>Your name:</th>
          <th><input name="name" maxlength="16"></th>
        </tr>
        <tr>
          <th>Your address:</th>
          <th><input name="address" maxlength="36"></th>
        </tr>       
        <!--
        <tr>
          <th colspan="2"><img src="i/<?=$capcha_file_1?>"></th>
        </tr>
        <tr>
          <th>Captcha #1:</th>
          <th><input name="captcha_1" placeholder="Type, what you see here"></th>
        </tr>
        <tr>
          <th colspan="2"><img src="i/<?=$capcha_file_2?>" title="You need a calculator =)"></th>
        </tr>
        <tr>
          <th>Captcha #2:</th>
          <th><input name="captcha_2" placeholder="Result of an EXPRESSION"></th>
        </tr>
        <tr>
          <th>Captcha #3:</th>
          <th><input name="captcha_3" placeholder="Result of an EXPRESSION in #2 + letters count in #1"></th>
        </tr>
        -->
        <tr>
          <th colspan="2" style='text-align: center'>
            <input type="submit" name="save" value="Send me some emercoins =)" class="btn btn-primary"/>
          </th>
        </tr>
      </table>
      
      <?if($err === true):?>
      <div class="alert alert-danger">
        <h3>
          <?=$out_text?>
        </h3>
      </div>
      <?elseif($err === false):?>
      <div class="alert alert-success">
        <h3>
          <?=$out_text?>
        </h3>
      </div>
      <?endif;?>
          
    </form>
    <?else:?>
      <a href='<?=$authUrl?>' class="btn btn-danger">LOGIN with GOOGLE+</a>
    <?endif;?>
    
    <? $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2); 
       if($lang != 'ru') $lang = 'en' ?>
    
    <h3 style="margin-top: 50px;">Want to recieve more ? <br/> <a href="/folding_home/about_<?=$lang?>.php">[click here]</a> </h3>
  
    <h2 style="margin-top: 50px;">People already received</h2>
    
    <table class="table">
      <tr>
        <th>Name</th>
        <th>Address</th>
        <th>IP</th>
        <th>Date</th>
        <th>Ammount</th>
      </tr>
      <?foreach($payments as $payment):?>
      <tr>
        <td><?=  htmlentities(strip_tags($payment['name']))?></td>
        <td><?=$payment['address']?></td>
        <td><?=$payment['IP']?></td>
        <td><?=$payment['date']?></td>
        <td><?=$payment['ammount']?> EMC</td>
      </tr>
      <?endforeach;?>
    </table>
</center>

</body>
</html>