<?php
if(!isset($application)) die("Can not call directly");

//Connecting
$connection = new PDO("mysql:host=localhost;dbname=emer_for", 'root', 'root');
$connection->query('SET NAMES "utf8"');

//Starting DB-based session
require(dirname(__FILE__).'/classes/session.php');
$session = new session($connection, 'session');