<?php
error_reporting(E_ALL^E_NOTICE^E_WARNING^E_STRICT^E_DEPRECATED);
ini_set("display_errors", true);
$run_service = true;
$application = 'EMERFOR';

//Initializing database and session
require(dirname(__FILE__).'/db.php');
require(dirname(__FILE__).'/db+.php');
require(dirname(__FILE__).'/classes/emercoin.php');
require(dirname(__FILE__).'/classes/emercoin.conf.php');
require dirname(__FILE__).'/classes/autopay.php';

$a = new autopay();
$a->execute_all();
echo 'AUTOPAY '.  date('Y.m.d H:i:s').' OK';