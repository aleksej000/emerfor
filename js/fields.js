var showFields = function() {
  $('input[data-type=date]').datepicker({format: 'yyyy-mm-dd'})
  $('input[data-type=int]').blur(function() {
    var v = parseInt($(this).val())
    if(isNaN(v)) v = 0
    $(this).val(v)
  })
  $('input[data-type=float]').blur(function() {
    var v = parseFloat($(this).val())
    if(isNaN(v)) v = 0
    $(this).val(v)
  })
}

