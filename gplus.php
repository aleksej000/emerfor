<?php
if(!isset($application)) die("Can not call directly");

$APPLICATION = 'emerfor';
$APPLICATION_SECRET = 'AIzaSyBommqYol24jOj7KqEsWJplFr6weIj10v0';
$CLIENT_ID = '737078742033-qf0bcnsntf55dtecsaq5solqsftjvmc3.apps.googleusercontent.com';
$EMAIL_ADDRESS = '737078742033-qf0bcnsntf55dtecsaq5solqsftjvmc3@developer.gserviceaccount.com';
$CLIENT_SECRET =	'Nb65wjCdc41QIX1mQBqQRJAz';
$REDIRECT_URI = 'http://emerfor.org/';
$JAVASCRIPT = 'http://emerfor.org/';

set_include_path(dirname(__FILE__).'/Google/');

require(dirname(__FILE__).'/Google/Client.php');
require(dirname(__FILE__).'/Google/Service/Plus.php');
require(dirname(__FILE__).'/Google/Service/Oauth2.php');

set_include_path(dirname(__FILE__).'/');

$client = new Google_Client();
$client->setApplicationName($APPLICATION);
$client->setApprovalPrompt (auto); //prompt consent screen only for first time

//*********** Replace with Your API Credentials **************
$client->setClientId($CLIENT_ID);
$client->setClientSecret($CLIENT_SECRET);
$client->setRedirectUri($REDIRECT_URI);
$client->setDeveloperKey($APPLICATION_SECRET);
//************************************************************

$client->setScopes(array('https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email'));

try {
  //$client->authenticate(); //after that authenticate user

  $plus = new Google_Service_Plus($client);

  $oauth2 = new Google_Service_Oauth2($client); // Call the OAuth2 class for get email address

  if (isset($_REQUEST['logout'])) {
    unset($_SESSION['gplus']);
  }

  if (isset($_GET['code'])) {
    $client->authenticate($_GET['code']);
    $_SESSION['gplus'] = $client->getAccessToken();
    
    $userinfo = $oauth2->userinfo->get();
    $_SESSION['gplus.id'] = $userinfo['id'];
    header('Location: http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF']);
  }

  if (isset($_SESSION['gplus'])) {
    $client->setAccessToken($_SESSION['gplus']);
  }

  if ($client->getAccessToken()) {
    $_SESSION['gplus'] = $client->getAccessToken();
  } else {
    $authUrl = $client->createAuthUrl();
  }
  
} catch (Exception $exc) {
    echo $exc->getTraceAsString();
}