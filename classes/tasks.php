<?php
require_once dirname(__FILE__).'/payments.php';
require_once dirname(__FILE__).'/emerpay.php';
/**
 * Class tasks
 *
 * @author Neo
 */
class tasks {
  public $table = 'fah_tasks';
  
  public function getFields() {
    return emerpay::getFields();
  }
  
  public function create($data, $module, $autopay_id = '') {
    $u = new users();
    $p = new payments();
    
    try {
      db::execute("START TRANSACTION");
      //Create task
      $dt = array(
          'module' => $module,
          'data' => serialize($data)
      );
      if($autopay_id != '') $dt['autopay_id'] = $autopay_id;
      db::insert($this->table, $dt);
      $task_id = db::getLastId($this->table);
      //Synhronize users
      $users = emerpay::getUsers($data);
      $ammount = 0;
      foreach($users as $username => $user) {
        $usernames[] = $username;
        $s_users[] = array('name' => $username, 'address' => str_replace('?', '', $user[0]));
        $ammount += (double)$user[1];
      }
      $u->synhronize($s_users);
      $final_users = $u->getByNames($usernames);
      //Creating payments
      foreach($final_users as $user)
        $p->create($task_id, $user['id'], $users[$user['name']][1]);
      //Updating task
      db::update($this->table, 
              array('ammount' => $ammount), 
              'id = '.(int)$task_id);
    } catch (Exception $exc) {
      db::execute("ROLLBACK");
      echo $exc->getTraceAsString();
    }
    
    db::execute("COMMIT");
  }
  
  public function get($id) {
    return db::getRow("SELECT * FROM $this->table WHERE id = $id");
  }
  
  public function lastId($id) {
    return db::getValue("SELECT MAX(id) FROM $this->table");
  }
  
  public function execute($id, $force_execute = false) {
    //Task module
    $module = db::getValue("SELECT `module` FROM $this->table WHERE id = $id");
    emerpay::loadModule($module);
    //Updating task data
    emerpay::getBeforeExecute($data, $id);
    //Executing payments
    $p = new payments();
    $payments = $p->getBy((int)$id);
    $errors = 0;
    foreach ($payments as $payment) {
      if( !$p->execute($payment['id'], $force_execute) ) $errors += 1;
    }
    if($errors > 0) {
      db::update($this->table, 
            "`error` = $errors", 
            'id = '.(int)$id);
    }
    else {
      $error = db::eacape('OK');
      //Updating task
      db::update($this->table, 
            "`executed` = NOW(), `error` = $errors", 
            'id = '.(int)$id);
    }
  }
  
  public function force_execute($id) {
    //Task module
    $module = db::getValue("SELECT `module` FROM $this->table WHERE id = $id");
    emerpay::loadModule($module);
    //Updating task data
    emerpay::getBeforeExecute($data, $id);
    //Executing payments
    $p = new payments();
    $payments = $p->getBy((int)$id);
    $errors = 0;
    foreach ($payments as $payment) {
      if( !$p->execute($payment['id'], true) ) $errors += 1;
    }
    if($errors > 0) {
      db::update($this->table, 
            "`error` = $errors", 
            'id = '.(int)$id);
    }
    else {
      $error = db::eacape('OK');
      //Updating task
      db::update($this->table, 
            "`executed` = NOW(), `error` = $errors", 
            'id = '.(int)$id);
    }
  }
  
  public function executed($id) {
    $res = db::getValue("SELECT module FROM $this->table WHERE id = $id");
    return (($res != null) && ($res != ''));
  }
  
  public function delete($id) {
    $p = new payments();
    //Deleting payments
    $payments = $p->getBy((int)$id);
    $error = db::eacape('OK');
    foreach ($payments as $payment) {
      $p->delete($payment['id']);
    }
    //Deleting task
    db::execute("DELETE FROM $this->table WHERE id = $id");
  }
  
  private function parse(&$tasks) {
    for($i = 0; $i < count($tasks); $i++) {
      $data = unserialize($tasks[$i]['data']);
      $tasks[$i]['data'] = $data;
      $module = $tasks[$i]['module'];
      emerpay::loadModule($module);
      $title = emerpay::getTitle($data);
      $tasks[$i]['name'] = "<b>$module:</b> $title";
    }
  }


  public function getList($condition = '1', $page = 1, $per_page = 25) {
    $condition .= " ORDER BY id DESC";
    $tasks = db::getPage("SELECT * FROM $this->table", $this->table, $condition, $page, $per_page);
    if($tasks) $this->parse($tasks);
    return $tasks;
  }
  
  public function getAllList($condition = '1') {
    $tasks = db::getList("SELECT * FROM $this->table WHERE 1 ORDER BY id DESC");
    if($tasks) $this->parse($tasks);
    return $tasks;
  }
  
  public function getListSimple($condition = '1') {
    return db::getList("SELECT * FROM $this->table WHERE 1 ORDER BY id DESC");
  }
}