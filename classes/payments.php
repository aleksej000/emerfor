<?php
require_once dirname(__FILE__).'/users.php';
require_once dirname(__FILE__).'/tasks.php';

/**
 * Class payment
 *
 * @author Neo
 */
class payments {
  private $table = 'fah_payments';
  
  public function create($task_id, $user_id, $ammount) {
    //echo " create($task_id, $user_id, $ammount) ";
    db::insert($this->table, array(
        'task_id' => $task_id,
        'user_id' => $user_id,
        'ammount' => $ammount
    ));
    return db::getLastId($this->table);
  }
  
  public function execute($id, $force_execute = false) {
    $account = 'Emer_for_free';//'FAH';
    $balance = emercoin::getAccauntBalance($account);
    $u = new users();
    
    $payment = $this->get($id);
    //If payment not executed
    if($force_execute || ($payment['executed'] == null) || ($payment['executed'] == '')) {
      
      $user = $u -> get($payment['user_id']);
      $user['address'] = str_replace('?', '', $user['address'] );
      
      try {
        if(($force_execute || ($payment['executed'] == null) || ($payment['executed'] == '')) && ($payment['ammount'] > 0) ) {
          //if(!TESTMODE) {
            if($balance >= ($payment['ammount'] + 0.1))
              emercoin::sendToAddress($user['address'], $payment['ammount'], $account);
            else {
              //Send email ********************************************
              require(dirname(__FILE__)."/pmailer/pmailer.php");
            
              $p = new pmailer(array());
              $p->From = "no-reply@emerfor.com";
              $p->FromName = "No reply";
              $p->Sender = "no-reply@emerfor.com";
              $p->Subject = "Insufficient funds";
              $p->CharSet = "UTF-8";
              $p->Body = "Insufficient funds ($balance EMC)";
              $email = db::getValue("SELECT `email` FROM `admin`");
              $p->AddAddress($email, "ADMIN");
              $p->IsSendmail();
              //$p->IsSMTP();
              $p->Send();
              //////////////////////////////////////////////////////////
              //throw new Exception("Insufficient funds");
              db::update($this->table, 
                       array('error' => 'Insufficient funds'), 
                      'id = '.(int)$id);
              return false;
            }
          //}
        }
        
        db::update($this->table, 
                "`executed` = NOW(), `error` = 'OK'", 
                'id = '.(int)$id);
        
        $fn_on_payment = emerpay::getOnPayment(array(
          'username' => $user['name'],
          'address' => $user['address'],
          'ammount' => $payment['ammount']
        ));
        
        return true;
      } catch (Exception $exc) {
        $error = jsonRPCClient::$error;
        //If wrong address, than don't save payment
        if((int)$error['code'] == -5) {
          db::execute("DELETE FROM $this->table WHERE id = ".(int)$id);
          return true;
        }
        else {
          $error = "Payment error [$error[message] (code: $error[code])]";
          db::update($this->table, 
                  array('error' => $error), 
                  'id = '.(int)$id);
        }
        return false;
      }
      
    }
    else {
      return true;
    }
  }
  
  public function get($id) {
    return db::getRow("SELECT * FROM $this->table WHERE id = $id");
  }
  
  public function delete($id) {
    db::execute("DELETE FROM $this->table WHERE id = $id");
  }
  
  public function getBy($task_id = '', $user_id = '') {
    $condition = '1';
    if($task_id != '') $condition .= " AND task_id = ".(int)$task_id;
    if($user_id != '') $condition .= " AND user_id = ".(int)$user_id;
    return db::getList("SELECT * FROM $this->table WHERE $condition");
  }
  
  private function parse(&$payments) {
    for($i = 0; $i < count($payments); $i++) {
      $data = unserialize($payments[$i]['task_data']);
      $payments[$i]['task_data'] = $data;
      $module = $payments[$i]['module'];
      emerpay::loadModule($module);
      $title = emerpay::getTitle($data);
      $payments[$i]['task'] = "<b>$module:</b> $title";
    }
  }
  
  public function getList($condition = '1', $page = 1, $per_page = 25) {
    $t = new tasks();
    $sql = <<<SQL
    SELECT TBL.task_id, T.module, T.data AS task_data, U.name AS username, U.address, TBL.*
    FROM $this->table TBL
    INNER JOIN fah_tasks T ON (TBL.task_id = T.id)
    INNER JOIN fah_users U ON (TBL.user_id = U.id)
SQL;
    $condition .= ' ORDER BY id DESC';
    $result = db::getPage($sql, $this->table, $condition, $page, $per_page);
    if($result) $this->parse($result);
    return $result;
  }
}