<?php

/**
 * Class users
 *
 * @author Neo
 */
class users {
  private $table = 'fah_users';
  
  public function get($id) {
    return db::getRow("SELECT * FROM $this->table WHERE id = ".(int)$id);
  }
  
  public function getList($condition = '1', $page = 1, $per_page = 25) {
    $condition .= " ORDER BY name";
    return db::getPage("SELECT * FROM $this->table", $this->table, $condition, $page, $per_page);
  }
  
  public function getAllList() {
    return db::getList("SELECT * FROM $this->table ORDER BY name");
    return $tasks;
  }
  
  public function synhronize($users) {
    foreach($users as $user) db::replace($this->table, $user);
  }
  
  public function getByNames($usernames) {
    for($i = 0; $i < count($usernames); $i++)
      $usernames[$i] = db::eacape ( $usernames[$i] );
    $usernames = implode(', ', $usernames);
    $sql = "SELECT * FROM $this->table WHERE name IN ($usernames)";
    return db::getList($sql);
  }
  
  public function getData($username, $unserialize = false) {
    $username = db::eacape($username);
    $result = db::getValue("SELECT `data` + 0 FROM $this->table WHERE name = $username");
    if($unserialize) $result = unserialize($result);
    return $result;
  }
  
  public function setData($username, $data, $serialize = false) {
    $username = db::eacape($username);
    if($serialize) $data = serialize($data);
    $data = db::eacape($data);
    return db::execute("UPDATE $this->table SET `data` = $data WHERE name = $username");
  }
}