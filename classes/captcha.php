<?php
if(!isset($application)) die("Can not call directly");
  
  /**
   * Generate captcha image (png)
   */
  function CAPTCHA__X($filename, $string, $height, $width, $fontfile, $scale = 1) {
    $colors = array(array(30, 80, 180), array(20, 160, 40), array(220, 40, 10));
    
    $im = imagecreatetruecolor($width * $scale, $height * $scale);
    imagealphablending($im, true);

    $GdBgColor = imagecolorallocate($im, 250, 250, 250);
    imagefilledrectangle($im, 0, 0, $width * $scale, $height * $scale, $GdBgColor);

    $color = $colors[mt_rand(0, sizeof($colors) - 1)];
    $GdFgColor = imagecolorallocate($im, $color[0], $color[1], $color[2]);

    $x = 20 * $scale;
    $y = round(($height * 0.75) * $scale);
    $length = strlen($string);

    for ($i = 0; $i < $length; $i++) {
        $degree = rand(-10, 10);
        $fontsize = rand(20, 25) * $scale;
        $letter = substr($string, $i, 1);
        $coords = imagettftext($im, $fontsize, $degree, $x, $y, $GdFgColor, $fontfile, $letter);
        $x += ($coords[2] - $x);
    }

    $k = rand(0, 100);
    $xp = $scale * 12 * rand(1, 3);
    for ($i = 0; $i < ($width * $scale); $i++)
        imagecopy($im, $im, $i - 1, sin($k + $i / $xp) * ($scale * 5), $i, 0, 1, $height * $scale);
    $k = rand(0, 100);
    $yp = $scale * 12 * rand(1, 3);
    for ($i = 0; $i < ($height * $scale); $i++)
        imagecopy($im, $im, sin($k + $i / $yp) * ($scale * 14), $i - 1, 0, $i, $width * $scale, 1);

    imagefilter($im, IMG_FILTER_GAUSSIAN_BLUR);

    $imResampled = imagecreatetruecolor($width, $height);
    imagecopyresampled($imResampled, $im, 0, 0, 0, 0, $width, $height, $width * $scale, $height * $scale);

    imagealphablending($imResampled, true);
    
    if(file_exists($filename)) unlink ($filename);
    $src_img = imagepng($imResampled, $filename);
    
    imagedestroy ( $imResampled );
    imagedestroy ( $im );

    chmod($filename, 0666);
    return $filename;
  };