<?php

/**
 * Class emerpay
 *
 * @author Neo
 */
class emerpay {
  
  public static $directory;
  public static $module;
  
  private static $data;
  
  public static function listModules() {
    $files = glob(self::$directory.'*.php');
    foreach($files as $filename) {
      $fn_parts = explode('/',$filename);
      $fn = str_replace('.php', '', $fn_parts[count($fn_parts) - 1]);
      $modules[$fn] = $fn;
    }
    return $modules;
  }
  
  public static function loadModule($module) {
    $emerpay_app = 'Emerpay';
    if(self::$module != $module)
      self::$data = require(self::$directory.$module.'.php');
    self::$module = $module;
  }
  
  private static function getParam($param, $data, $param1, $param2) {
    if(!isset(self::$data)) throw new Exception('The module is not loaded');
    if(!isset(self::$data[$param])) throw new Exception('The "'.$param.'" param of module "'.self::$module.'" does not exist');
    if(is_callable(self::$data[$param])) {
      return call_user_func(self::$data[$param], $data, $param1, $param2);
    }
    else {
      return self::$data[$param];
    }
  }

  public static function getUsers($data) {
    return self::getParam('users', $data);
  }
  
  public static function getFields() {
    return self::getParam('fields');
  }
  
  public static function getAutoFields() {
    return self::getParam('autopay_fields');
  }
  
  public static function getFieldTypes() {
    return self::getParam('field_types');
  }
  
  public static function getTitle($data) {
    return self::getParam('task_title', $data);
  }
  
  public static function getAutoTitle($data) {
    return self::getParam('autopay_title', $data);
  }
  
  public static function getOnPayment($data) {
    return self::getParam('on_payment', $data);
  }
  
  public static function getBeforeExecute($data, $id) {
    return self::getParam('fn_before_execute', $data, $id);
  }
  
  public static function getOnAutoCreateTask($data, $autopay_id) {
    return self::getParam('fn_on_auto_create', $data, $autopay_id);
  }
  
}

emerpay::$directory = dirname(__FILE__).'/../emerpay/emerpay/';