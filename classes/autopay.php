<?php
require_once dirname(__FILE__).'/tasks.php';
require_once dirname(__FILE__).'/emerpay.php';
/**
 * Class tasks
 *
 * @author Neo
 */
class autopay {
  public $table = 'fah_autopay';
  
  public function get($id) {
    return db::getRow("SELECT * FROM $this->table WHERE id = $id");
  }
  
  public function delete($id) {
    //Deleting record
    db::execute("DELETE FROM $this->table WHERE id = $id");
  }
  
  public function create($data, $module, $interval) {
    db::insert($this->table, array(
        'data' => serialize($data),
        'module' => $module,
        'interval' => $interval
    ));
  }
  
  public function execute($id, $record) {
    $t = new tasks();
    emerpay::loadModule($record['module']);
    $data = emerpay::getOnAutoCreateTask(unserialize($record['data']), $id);
    $t->create($data, $record['module'], $id);
    $task_id = $t->lastId();
    $t->execute($task_id);
    db::update($this->table, "`last_ex` = now(), ex_times = ex_times + 1", "id = $id");
  }
  
  public function run($id) {
    db::update($this->table, array('active' => 1), 'id = '.$id);
  }
  
  public function stop($id) {
    db::update($this->table, array('active' => 0), 'id = '.$id);
  }
  
  public function execute_all() {
    $this->execute_all_period('day');
    $this->execute_all_period('week');
    $this->execute_all_period('month');
  }
  
  public function execute_all_period($period) {
    $condition = '1';
    switch ($period) {
      case 'day':
        $condition = " ( (last_ex IS NULL) OR (DATEDIFF(CURRENT_DATE(), last_ex) >= 1) )";
      break;
      case 'week':
        $condition = " ( (last_ex IS NULL) OR (DATEDIFF(CURRENT_DATE(), last_ex) >= 7) )";
      break;
      case 'month':
        $condition = " ( (last_ex IS NULL) OR (DATEDIFF(CURRENT_DATE(), last_ex) >= 30) )";
      break;
    }
    $autopays = db::getList("SELECT * FROM $this->table ".
            "where $condition AND `interval` = ".db::eacape($period));
    //echo "SELECT * FROM $this->table where $condition AND `interval` = ".db::eacape($period);
    //var_dump($autopays);
    foreach($autopays as $autopay) {
      $this->execute($autopay['id'], $autopay);
    }
  }


  public function getList() {
    $list = db::getList("SELECT * FROM $this->table ORDER BY id DESC ");
    if($list) $this->parse($list);
    return $list;
  }
  
  private function parse(&$autopays) {
    for($i = 0; $i < count($autopays); $i++) {
      $data = unserialize($autopays[$i]['data']);
      $module = $autopays[$i]['module'];
      emerpay::loadModule($module);
      $title = emerpay::getAutoTitle($data);
      $autopays[$i]['name'] = "<b>$module</b> $title";
    }
  }
  
}