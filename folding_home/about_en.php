<!DOCTYPE html>
<html lang="en">
<head>    
  <meta charset="utf-8">
  <title>About Folding@Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="http://emerfor.org/favicon.ico" />
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
</head>

<body>
<ul class="nav nav-tabs">
  <li class="active">
    <a href="/folding_home/about_en.php">About</a>
  </li>
  <li>
    <a href="/folding_home/stats_en.php">Statistics</a>
  </li>
</ul>
  
<h3>What is Folding @ Home?</h3>

<p>
Folding @ Home is a distributed computing project for disease research that simulates protein folding, computational drug design, and other types of molecular dynamics. Launched in October 1, 2000 by scientists from Stanford University, the project uses the idle processing resources of thousands of personal computers owned by volunteers who have installed the software on their systems. Both CPU and GPU resources are employed, with GPU being the most effective. The F@H software client is available for most major operating systems including Windows (x86 and x64), Mac OS X (Intel processors), Linux (x86 and x64).
</p> 
<p>
  <i>What benefit from the project?</i>
</p> 
<p>
Immediate goal - getting a clearer idea of the diseases caused by defective proteins. Study of proteins is of significant academic interest with major implications for medical research into Alzheimer's disease, Huntington's disease, many forms of cancer, and other diseases including Parkinson's disease. To a lesser extent, Folding @ Home also tries to predict a protein's final structure and determine how other molecules may interact with it, which has applications in drug design and nanotechnology.
</p> 
<p>
  <i>How can I join?</i>
</p> 
<p>
  Go to the <a href="http://folding.stanford.edu/">http://folding.stanford.edu/</a> and click the START FOLDING link at the bottom of the page.
</p> 
<p style="font-weight: bold;">
1. Download, install and run the Folding @ Home client software. The client will open a browser tab for you to enter your details.<br/>
2. Click on "Configure an Identity" (do not choose "Contribute Anonymously")<br/>
3. Enter the following details:<br/>
</p> 
<p>
Nickname: <b>username_&lt;EmercoinAddress&gt;</b> (e.g. IloveEMC_EW3uMrGWQrq8hKZN9iwkrSarebL4ByuwDZ)

Team: #<b>43003</b> (#<b>43003</b> is Team Transhumans - the team has existed for some time but older participants don't have an advantage over new)

Passkey: Choose <a href="http://fah-web.stanford.edu/cgi-bin/getpasskey.py">"Get a passkey"</a> to get one. Passkeys are used as an extra level of security and will also allow you to earn bonus points. Use the exact same name that you used above, and have your passkey sent to you by email.
</p> 
<p>
Remember to click Save when you have entered all details.
</p> 
<p>
Now you can choose your computational load ( Off - Light Idle - Idle - Light - Medium - Full )
You can also choose what kind of disease you want to help eradicate.
The interface will also indicate your Points Per Day. The first time you run the software this will show as "Unknown".
</p> 
<p>
On the same page is a table of devices found on your system. Basically, the system will immediately recognize all devices on your computer that can do the folding calculations. If you have an AMD graphics card it is necessary to move the slider switch to Full load before the card will contribute. For nVidia cards this is not required as they are run immediately.
</p> 
<p>
  <i>How can I tell what my computer is working on?</i>
</p> 
<p>
On the page it will say which project you are contributing towards. e.g. "You are contributing to project 6364". You can learn about the specifics of the project including scientific values and rewards by looking it up at <a href="http://fah-web.stanford.edu/psummaryC.html">http://fah-web.stanford.edu/psummaryC.html</a> You can also see a cool 3D model of the molecule being processed by right-clicking the F@H icon in the system tray and choosing the "Protein Viewer" option.
</p> 
<p>
<p>
  <i>How much will I earn?</i>
</p> 
<p>
HD7970 graphics card produces about 70,000 per day. Processors range from 1,000 to 80,000 (dual Xeon system - 32 cores). nVidia card gives 5 times more than CPU but are inferior to AMD graphics cards. Bitcoin ASIC, of course, do not apply.
 </p> 
<p>
  <i>The team decided to equate 1000 F@H points to one EMC.</i>
</p> 
<p>
The team plans to remunerate automatically one once a month, but while the system is being tested we aim to remunerate once a day.
</p> 
<p>
Join now and start earning Emercoins while helping to eradicate some of the world's most terrible diseases!
</p> 
<p>
Thank you!
</p> 
<p>
If you require assistance then please provide your F@H username and configuration of machines with your comment, so it is easier to assist.
</p>
<p>
I should say that I am not the developer of this project but have assisted in translation only. I will be happy to try and answer any questions but may need to defer to someone more involved.
</p> 
<p>
  NOTE: The Transhumans F@H stats can be found at <a href="http://folding.extremeoverclocking.com/user_list.php?s=&t=43003">http://folding.extremeoverclocking.com/user_list.php?s=&t=43003</a> and you can see, several using their EMC address.
</p> 
<center><a href='/'>BACK</a></center>

</body>
</html>