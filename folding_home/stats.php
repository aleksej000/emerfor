<?php
if(!isset($application)) die('Dirrect access not allowed');

require_once dirname(__FILE__).'/../db.php';
require_once dirname(__FILE__).'/../db+.php';

$sql = <<<SQL
SELECT T.*, total/if(days < 1, 1, days ) AS daily
FROM
(SELECT U.name, SUM(P.ammount) AS total , 
MIN(P.executed), MAX(P.executed),  
@days := DATEDIFF(MAX(P.executed), MIN(P.executed)) AS days
FROM `fah_payments` P 
INNER JOIN `fah_users` U ON (P.user_id = U.id)
WHERE NOT (P.executed IS NULL)
GROUP BY U.id) T
SQL;

if($order == 'daily')
  $sql .= " ORDER BY daily DESC";
else
  $sql .= " ORDER BY total DESC";

$stats = db::getList($sql);

for ($i = 0; $i < count($stats); $i++) {
  $stats[$i]['place'] = $i + 1;
  $stats[$i]['total'] = round($stats[$i]['total'], 6);
  $stats[$i]['daily'] = round($stats[$i]['daily'], 6);
}
?>