<?php
error_reporting(E_ALL^E_NOTICE^E_WARNING^E_STRICT^E_DEPRECATED);
ini_set("display_errors", true);

$application = 'Stats';

$order = $_GET['order'];
if(!isset($order)) $order = 'total';

require('stats.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>    
  <meta charset="utf-8">
  <title>Статистика Folding@Home</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="http://emerfor.org/favicon.ico" />
  <link href="/css/bootstrap.min.css" rel="stylesheet">
  <script src="/js/jquery.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <style>
    .table {
      min-width: 800px;
      width: 50%;
    }
    a.active {
      text-decoration: underline;
      color: #269abc;
    }
  </style>
</head>
<body>
  
<ul class="nav nav-tabs">
  <li>
    <a href="/folding_home/about_ru.php">О системе Folding@Home</a>
  </li>
  <li class="active">
    <a href="/folding_home/stats_ru.php">Статистика</a>
  </li>
</ul>
  
<center>
  
    <table class="table">
      <tr>
        <th>Место</th>
        <th>Пользователь</th>
        <th>
          <?if($order == 'total'):?>
          <a href="?order=total"  class="active" >Всего заработано (EMC)</a> <i class="glyphicon glyphicon-arrow-down"></i>
          <?else:?>
          <a href="?order=total">Всего заработано (EMC)</a>
          <?endif;?>
        </th>
        <th>
          <?if($order == 'daily'):?>
          <a href="?order=daily"  class="active" >В среднем за день (EMC)</a> <i class="glyphicon glyphicon-arrow-down"></i>
          <?else:?>
          <a href="?order=daily">В среднем за день (EMC)</a>
          <?endif;?>
        </th>
      </tr>
      <?foreach($stats as $stat):?>
      <tr>
        <td><?=  htmlentities(strip_tags($stat['place']))?></td>
        <td><?=$stat['name']?></td>
        <td><?=$stat['total']?></td>
        <td><?=$stat['daily']?></td>
      </tr>
      <?endforeach;?>
    </table>
  
</center>

</body>
</html>